# Ini menggunakan module weather-api dari pyPI

# Install via terminal : pip install weather-api / pip3 install weather-api
# Requirements.txt ditambah : weather-api==1.0.3

# Lihat module di sini: https://pypi.org/project/weather-api/

import linebot
import os.path
import sys

from mtranslate import translate
from weather import Weather, Unit

# Bagian bawah ini akan merujuk ke parent directory (yang satu folder dengan Main)
# Tujuannya supaya bisa import objek line_bot_api di botSetting
from resources.setup import line_bot_api

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")

from linebot.models import (TemplateSendMessage, CarouselTemplate, CarouselColumn, \
                                URITemplateAction, TextSendMessage)

def terjemahkan(kalimatYangMauDiterjemahkan, kodeBahasaTujuan='id'):
    hasilTerjemahan = translate(kalimatYangMauDiterjemahkan, kodeBahasaTujuan)
    return hasilTerjemahan


def tampilHasilCuaca(kota, keterangan, tanggal, panas, dingin, eventInteraksi):
    # Menampilkan dengan CarouselTemplate
    # Detil penggunaan syntax ada di https://github.com/line/line-bot-sdk-python
    # Cari aja "CarouselTemplate"

    # Carousel itu adalah suatu rich menu dari LINE yang dapat di scroll
    # Rich menu yang disediakan dari LINE dan bagaimana penampilannya, ada di
    # https://developers.line.me/en/docs/messaging-api/message-types/
    # Rich menu ada button, carousel, tombol konfirmasi, dan lainnya

    # Tetapi setiap rich menu punya syarat dan ketentuan masing - masing
    # Bisa dilihat untuk Carousel di https://developers.line.me/en/docs/messaging-api/reference/#carousel

    # Variabel alt_text adalah judul yang ditampilkan apabila menu tidak dapat ditampilkan
    # Misalnya karena pakai LINE untuk PC, atau LINE versi lama

    # URITemplateAction , sesuai namanya, ketika tombol ditekan, dia akan melakukan aksi membuka sebuah website
    # Selain URITemplateAction, ada pula MessageTemplateAction

    hasilCuaca = TemplateSendMessage(
        alt_text='Prakiraan Cuaca!',
        template=CarouselTemplate(
            columns=[CarouselColumn(
                title="Cuaca " + kota + " Hari ini!",
                text=tanggal[0] + "\n" + terjemahkan(keterangan[0]) + "\n" + "Suhu: " + dingin[
                    0] + " ~ " + panas[0] + " °C",
                actions=[
                    URITemplateAction(label='Yahoo! Weather', uri='http://weather.yahoo.com')]),

                CarouselColumn(
                    title="Cuaca " + kota + " Besok!",
                    text=tanggal[1] + "\n" + terjemahkan(keterangan[1]) + "\n" + "Suhu: " + dingin[
                        1] + " ~ " + panas[1] + " °C",
                    actions=[
                        URITemplateAction(label='Yahoo! Weather', uri='http://weather.yahoo.com')]),

                CarouselColumn(
                    title="Cuaca " + kota + " Lusa!",
                    text=tanggal[2] + "\n" + terjemahkan(keterangan[2]) + "\n" + "Suhu: " + dingin[
                        2] + " ~ " + panas[2] + " °C",
                    actions=[
                        URITemplateAction(label='Yahoo! Weather', uri='http://weather.yahoo.com')])

            ]))

    line_bot_api.reply_message(eventInteraksi.reply_token, [hasilCuaca, TextSendMessage(
        text="Berikut peramalan cuaca di " + kota + " \n" + "Persiapkanlah diri")])


def prosesCuaca(lokasi):
    weather = Weather(unit=Unit.CELSIUS)

    cariTempat = weather.lookup_by_location(lokasi)

    keteranganCuaca = []
    tanggalCuaca = []
    suhuTinggi = []
    suhuRendah = []

    try:
        ramalCuaca = cariTempat.forecast

    except linebot.exceptions.LineBotApiError:  # Kalau misalnya pencarian cuaca terjadi error
        return "error"

    else:
        for i in range(0, 3):
            keteranganCuaca.append(ramalCuaca[i].text)
            tanggalCuaca.append(ramalCuaca[i].date)
            suhuTinggi.append(ramalCuaca[i].high)
            suhuRendah.append(ramalCuaca[i].low)

        return [keteranganCuaca, tanggalCuaca, suhuTinggi, suhuRendah]


def cariLokasi(eventInteraksi):
    alamat = eventInteraksi.message.address
    alamat_split = alamat.split(', ')

    if len(alamat_split) == 1 or len(alamat_split) == 2:
        lokasi = alamat_split[0]
    elif len(alamat_split) == 3:
        lokasi = alamat_split[-2]
    else:
        lokasi = alamat_split[-3]

    if len(lokasi) > 20:
        return 'error'
    else:
        return lokasi


def get_cuaca(eventInteraksi):

    tempatTujuan = cariLokasi(eventInteraksi)  # cariLokasi() ada di atas

    if tempatTujuan == 'error':  # Exception jila error
        line_bot_api.reply_message(eventInteraksi.reply_token, TextSendMessage(
            text="Pencarian cuaca gagal!, cobalah untuk mengganti lokasi & periksalah kembali koneksi Anda!"))

    else:  # Jika tidak error, memanggil tampilHasilCuaca() di atas method ini
        dataCuaca = prosesCuaca(tempatTujuan)  # prosesCuaca() ada di atas
        tampilHasilCuaca(tempatTujuan, dataCuaca[0], dataCuaca[1], dataCuaca[2], dataCuaca[3], eventInteraksi)  # Memanggil fungsi tampilHasilCuaca() berada pada atas fungsi ini
