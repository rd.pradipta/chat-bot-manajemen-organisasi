# File ini untuk dropbox (Mengatur File)
# Isinya masih coming soon

"""Upload the contents of your Downloads folder to Dropbox.

This is an example app for API v2.
"""

from __future__ import print_function

import argparse
import contextlib
import datetime
import os
import six
import sys
import time
import unicodedata

import dropbox
from dropbox.files import WriteMode
from dropbox.exceptions import ApiError, AuthError

from dropbox import sharing

if sys.version.startswith('2'):
    input = raw_input  # noqa: E501,F821; pylint: disable=redefined-builtin,undefined-variable,useless-suppression

TOKEN = 'Z7kdntbfzEsAAAAAAAADyz7XGgAIn99q9kb7moRjCs0-ZcGcPv77tKxZ1eKeMf2L'

if (len(TOKEN) == 0):
    sys.exit("ERROR: Access Token tidak ada")

print("Creating a Dropbox object...")

dbx = dropbox.Dropbox(TOKEN)

try:
    dbx.users_get_current_account()
except AuthError as err:
    sys.exit("ERROR: Invalid access token; try re-generating an "
        "access token from the app console on the web.")

def upload_dbx(dbx, fullname, path_local, overwrite=True):

    """Upload a file.
    Return URL dari file jika berhasil, atau pesan error lain jika gagal

    fullname = full relative path (pakai module os)
    path_local = path folder yang akan disimpan dalam dropbox
    """

    path = path_local

    mode = (dropbox.files.WriteMode.overwrite
            if overwrite
            else dropbox.files.WriteMode.add)
    mtime = os.path.getmtime(fullname)
    with open(fullname, 'rb') as f:
        data = f.read()
    with stopwatch('upload %d bytes' % len(data)):
        try:
            dbx.files_upload(
                data, path, mode,
                client_modified=datetime.datetime(*time.gmtime(mtime)[:6]),
                mute=True)
        except dropbox.exceptions.ApiError as err:
            print('*** API error', err)
            return 'Terjadi masalah, gagal mengupload file Anda. Jika Anda merasa ini bug, mohon hubungi Admin melalui /bantuan'

    print('File berhasil terupload')
    print('Mencoba membuat link shareable')

    try:
        arg = sharing.CreateSharedLinkWithSettingsArg(path)
        file = dbx.request(
            sharing.create_shared_link_with_settings,
            'sharing',
            arg,
            None,
        )
    except dropbox.exceptions.ApiError:
        print('Gagal mendapatkan link share, Tapi file telah terupload')
        print('Mengambil link share')
        
        arg = sharing.ListSharedLinksArg(path,None,None)
        r = dbx.request(
            sharing.list_shared_links,
            'sharing',
            arg,
            None,
        )
        return 'Revisi file berhasil diupload, Silakan klik link berikut untuk mulai mengunduh\n' + '\n' + r.links[0].url
    else:
        print('Link share berhasil dibuat')
        return ['File berhasil Orgo simpan, Silakan klik link berikut untuk mulai mengunduh\n' + '\n' + file.url , file.url]

@contextlib.contextmanager
def stopwatch(message):
    """Context manager to print how long a block of code took."""
    t0 = time.time()
    try:
        yield
    finally:
        t1 = time.time()
        print('Total elapsed time for %s: %.3f' % (message, t1 - t0))