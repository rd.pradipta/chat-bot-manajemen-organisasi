# Modul terjemahan, bisa untuk menerjemahkan API atau lainnya

# Ini menggunakan module mtranslate , installnya:
# pip install mtranslate / pip3 install mtranslate

# Requirements.txt ditambah:
# mtranslate==1.6

# Source code:
# https://github.com/mouuff/mtranslate/

import sys
import os.path

from mtranslate import translate

''' Bagian bawah ini mengimport botSetting dan botHelp, tapi dengan cara khusus
hal ini agar file fitur_terjemahan.py tetap dapat di-run dan tidak membuat kebingungan
'''

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")

from resources.setup import line_bot_api
from linebot.models import TextSendMessage

''' Ceritanya ini mau menerjemahkan antar bahasa, default bahasa tujuan adalah bahasa Indonesia
Mungkin bisa berguna untuk menerjemahkan API yang pakai bahasa Inggris supaya ditampilkan dalam bahasa Indonesia
Bahasa awal bisa di deteksi otomatis, tapi bahasa tujuan harus diberi tahu kode bahasanya
'''

def olahTerjemahan(kalimatYangMauDiterjemahkan, kodeBahasaTujuan='id'):

    print("Memulai menerjemahkan")
    hasilTerjemahan = translate(kalimatYangMauDiterjemahkan, kodeBahasaTujuan)
    print (hasilTerjemahan)
    return hasilTerjemahan

def olahKalimat(masukan_raw):

    daftarBahasa = {'indonesia':'id',   'indo':'id',
                    'inggris':'en',     'english':'en',
                    'jepang':'ja',      'japan':'ja',
                    'portugis':'pt',
                    'rusia':'ru',       'russian':'ru',
                    'spanyol':'es',     'korea':'ko',
                    'swedia':'sv',
                    'thailand':'th',    'thai':'th',
                    'vietnam':'vi',
                    'arab':'ar',        'arabic':'ar',
                    'perancis':'fr',    'prancis':'fr',
                    'jerman':'de',      'german':'de',
                    'italia':'it',      'itali':'it'}

    for i in list(daftarBahasa):
        if i in masukan_raw and 'dari' in masukan_raw:
            listKalimat = masukan_raw.split(' ')
            idx = listKalimat.index('dari')
            kalimatDiminta = ' '.join(listKalimat[idx+1:])
            hasil = olahTerjemahan(kalimatDiminta,daftarBahasa[i])

            return [kalimatDiminta,hasil]

        elif i in masukan_raw and 'dari' not in masukan_raw:
            listKalimat = masukan_raw.split(' ')

            try:
                idx = listKalimat.index(i+"nya")
            except ValueError:
                idx = listKalimat.index(i)

            kalimatDiminta = ' '.join(listKalimat[idx+1:])
            hasil = olahTerjemahan(kalimatDiminta,daftarBahasa[i])

            return [kalimatDiminta,hasil]

    return "error"

def getTerjemahan(eventInteraksi,masukan_raw):
    print("Masuk fungsi get_terjemahan")
    hasilOlah = olahKalimat(masukan_raw)
    print("Hasil translate: " + str(hasilOlah))

    if hasilOlah == "error":
        line_bot_api.reply_message(eventInteraksi.reply_token, TextSendMessage(text="Mohon maaf, terjadi kesalahan dalam menerjemahkan. Bahasa tidak didukung atau perintah belum dikenali"))

    else:
        balasan = "Hasil terjemahan " + hasilOlah[0] + "\n" + hasilOlah[1] + "\n" + "Semoga hasilnya memuaskan ya!"

        line_bot_api.reply_message(eventInteraksi.reply_token, TextSendMessage(text=balasan))

# List kode bahasa (Lihatnya yang ISO-639)
# https://docs.oracle.com/cd/E13214_01/wli/docs92/xref/xqisocodes.html
