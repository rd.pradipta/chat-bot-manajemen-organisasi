# Mungkin di sini nanti bisa diisi dengan kumpulan teks untuk bantuan dari setiap fitur (?)

ERROR_NON_GROUP = "Fitur ini hanya bisa dilakukan dalam Grup. Multichat & Personal Chat (PM) tidak didukung"
ERROR_ORGANIZATION_NOT_REGISTERED = \
'''Permintaan gagal untuk dilakukan!
Grup/organisasi belum terdaftar di Orgo.

Cara mendaftar:
1. Undang Orgo ke Grup Anda
2. Masuk ke Menu dengan mengetik /menu pada obrolan grup (Jangan lupa tanda /)
3. Tekan TAMBAH ORG./GRUP
4. Ikuti Prosesnya hingga selesai

Catatan:
Setelah berhasil, semua anggota grup diwajibkan menekan DAFTAR & SINKRON
Hal ini agar setiap orang terdata oleh Orgo sebagai anggota grup'''

ERROR_NOT_JOINED_ANY_ORG = \
'''Permintaan gagal untuk dilakukan!
Saat ini Anda bukanlah anggota grup/organisasi manapun.
Cara mendaftar:

Cara 1:
Ketikkan /sync (Jangan lupa tanda /)

Cara 2:
Ketikkan /menu (Jangan lupa tanda /)
Tekan DAFTAR & SINKRON

Mendaftar hanya dapat dilakukan dalam grup
Selesai, mudah bukan!'''

ERROR_NO_ACCESS = \
'''Permintaan gagal untuk dilakukan!
Saat ini Anda bukanlah anggota grup/organisasi ini.
Cara mendaftar:

Cara 1:
Ketikkan /sync (Jangan lupa tanda /)

Cara 2:
Ketikkan /menu (Jangan lupa tanda /)
Tekan DAFTAR & SINKRON

Mendaftar hanya dapat dilakukan dalam grup
Selesai, mudah bukan!'''

ERROR_WRONG_FORMAT = "Mohon maaf, format yang Anda masukkan salah, silakan coba kembali menggunakan format yang benar. Ketik BATAL untuk membatalkan aksi ini"

BANTUAN_DASAR = '''Halo, ada yang bisa Orgo bantu ?
Silakan ketik:

/bantuan untuk menuju Bantuan
/menu untuk menuju Menu

(Jangan lupa tanda / nya)'''

feedbackOrgo = '''Apakah Anda memiliki pesan yang ingin disampaikan kepada Admin agar Orgo menjadi lebih baik lagi?
Anda dapat menyampaikan kritik/saran/pesan lain kepada Admin Orgo dengan mudah!

Cukup dengan mengetikkan:
/kirim_pesan <spasi> Pesan yang ingin disampaikan

Contoh:
/kirim_pesan Semoga Orgo menjadi lebih baik ke depannya !

(Jangan lupa tanda / nya ya!)
Pesan dari Anda akan segera sampai ke Admin Orgo.
Tenang saja, pesan Anda bersifat anonim dan akan terjaga kerahasiaannya

Orgo tunggu kritik/saran/pesan dari Anda
'''

aturGrupOrgo = ''' Mengatur Grup/Organisasi Anda

Orgo adalah chat bot yang hadir untuk membantu mengelola grup/organisasi \
agar dapat berjalan dengan efektif dan efisien.
Untuk dapat memulai menggunakan Orgo, ikuti langkah berikut:

1. Tambahkan Orgo sebagai teman
2. Undang Orgo ke grup LINE
3. Ketikkan /menu lalu pilih "Daftar Org./Grup
(Dapat pula dengan kata kunci /buat_grup)
4. Bot akan meminta Anda memasukkan nama dan deskripsi grup
5. Ikuti prosesnya hingga selesai

*Organisasi/Grup berhasil terdaftar di Orgo*
Setelah itu, semua anggota grup harus sinkron dengan Orgo, agar \
para anggota grup terdaftar sebagai anggota grup & tidak tertinggal \
informasi baru, tahapannya:

1. Ketikkan /menu lalu pilih "Daftar & Sinkron"
(Dapat pula dengan kata kunci /sync)
2. Sinkron selesai

Semua tahap harus dilakukan agar Anda dapat menggunakan fitur Orgo \
secara penuh'''

aturJadwalOrgo = '''Mengatur Jadwal Bersama Orgo

Anda dapat menambah jadwal dengan detail:
> Nama
> Lokasi
> Tanggal
> Keterangan

1. Membuat Jadwal
Fitur ini hanya tersedia melalui grup chat. \
Melalui fitur ini, setiap grup/organisasi dapat menambahkan jadwal.
Fitur ini dapat diakses dengan 2 cara:

A. Mengetik /menu lalu pilih "Tambah" di menu mengelola jadwal
B. Mengetik /buat_jadwal di grup chat
Setelah itu, ikuti langkah yang diberikan hingga selesai

2. Melihat Jadwal
Anda dapat melihat semua jadwal yang telah dibuat di grup/organisasi Anda
Fitur ini dapat diakses dengan berbagai pilihan cara:

A. Mengetik /menu lalu pilih "Lihat Jadwal" di menu mengelola jadwal
B. Memilih "Jadwal" di personal chat
C. Mengetik /lihat_jadwal di grup atau personal chat
D. Mengetik /lihat_semua di grup atau personal chat

Catatan:
*Pastikan grup LINE yang ingin diproses \
telah terdaftar di Orgo, dan Anda sebagai anggota telah tersinkron sebagai anggota \
dari grup tersebut
*Tampilan /menu akan berbeda jika tidak diakses dalam grup'''

aturPengumumanOrgo = '''Mengatur Pengumuman Bersama Orgo

Anda dapat menambah pengumuman dengan detail:
> Nama
> Tingkat Kepentingan
> Nama Pembuat
> Tanggal
> Keterangan

1. Membuat Pengumuman
Fitur ini hanya tersedia melalui grup chat. \
Melalui fitur ini, setiap grup/organisasi dapat menambahkan pengumuman.
Fitur ini dapat diakses dengan 2 cara:

A. Mengetik /menu lalu pilih "Tambah" di menu mengelola pengumuman
B. Mengetik /buat_pengumuman di grup chat
Setelah itu, ikuti langkah yang diberikan hingga selesai

2. Melihat Pengumuman
Anda dapat melihat semua pengumuman yang telah dibuat di grup/organisasi Anda
Fitur ini dapat diakses dengan berbagai pilihan cara:

A. Mengetik /menu lalu pilih "Lihat Pengumuman" di menu mengelola pengumuman
B. Memilih "Pengumuman" di personal chat
C. Mengetik /lihat_pengumuman di grup atau personal chat
D. Mengetik /lihat_semua di grup atau personal chat

Catatan:
*Pastikan grup LINE yang ingin diproses \
telah terdaftar di Orgo, dan Anda sebagai anggota telah tersinkron sebagai anggota \
dari grup tersebut.
*Tampilan /menu akan berbeda jika tidak diakses dalam grup'''

aturProkerOrgo = '''Mengatur Program Kerja Bersama Orgo

Anda dapat menambah program kerja dengan detail:
> Nama
> Jangka Waktu
> Penanggung Jawab
> Keterangan

1. Membuat Proker (Program Kerja)
Fitur ini hanya tersedia melalui grup chat. \
Melalui fitur ini, setiap grup/organisasi dapat menambahkan proker.
Fitur ini dapat diakses dengan 2 cara:

A. Mengetik /menu lalu pilih "Tambah" di menu mengelola proker
B. Mengetik /buat_proker di grup chat
Setelah itu, ikuti langkah yang diberikan hingga selesai
    
2. Melihat Proker (Program Kerja)
Anda dapat melihat semua proker yang telah dibuat di grup/organisasi Anda
Fitur ini dapat diakses dengan berbagai pilihan cara:

A. Mengetik /menu lalu pilih "Lihat Proker" di menu mengelola pengumuman
B. Mengetik /lihat_proker di grup atau personal chat
C. Mengetik /lihat_semua di grup atau personal chat

Catatan:
*Pastikan grup LINE yang ingin diproses \
telah terdaftar di Orgo, dan Anda sebagai anggota telah tersinkron sebagai anggota \
dari grup tersebut.
*Tampilan /menu akan berbeda jika tidak diakses dalam grup'''

aksiUploadDokumen = '''Silakan kirimkan dokumen melalui chat, Orgo akan \
memberikan link unduh dari file tersebut kepada para anggota grup, sehingga bisa \
diunduh kembali saat diperlukan'''

aturDokumenOrgo = '''Mengatur Dokumen Bersama Orgo

1. Menyimpan Dokumen
Fitur ini hanya tersedia melalui grup chat. \
Melalui fitur ini, setiap grup/organisasi dapat menyimpan dokumen
Cukup dengan kirim dokumen melalui chat, Orgo akan memberikan link unduh \


2. Melihat Daftar Dokumen
Anda dapat melihat semua dokumen yang telah dibuat di grup/organisasi Anda
Fitur ini dapat diakses dengan berbagai pilihan cara:

A. Mengetik /menu lalu pilih "Lihat Dokumen" di menu mengelola pengumuman
B. Mengetik /lihat_dokumen di grup atau personal chat
C. Mengetik /lihat_semua di grup atau personal chat

Catatan:
*Pastikan grup LINE yang ingin diproses \
telah terdaftar di Orgo, dan Anda sebagai anggota telah tersinkron sebagai anggota \
dari grup tersebut.
*Tampilan /menu akan berbeda jika tidak diakses dalam grup'''

infoLainOrgo = '''**Fitur lain Orgo**

==========

> Melihat daftar grup yang Anda ikuti beserta aksi lain yang tersedia untuk grup itu
(Melihat jadwal, pengumuman, dan lainnya)

1. Memilih "Organisasi Terdaftar" di personal chat, atau kata kunci /lihat_semua di grup atau personal chat
2. Pilih grup/organisasi yang ingin diproses

==========

> Kirim Pesan Anonim kepada grup, ini berguna jika Anda ingin menyampaikan pendapat, \
pesan, atau hal lain, tetapi malu untuk menyampaikan.
Mode ini adalah mode pesan rahasia, Orgo juga bisa \
mengirimkan semua chat yang sedang aktif ke profil Anda, dengan kata lain, Anda \
dapat menjadi "Silent Reader" & berinteraksi secara anonim tanpa membuka grup

1. Masuk ke mode melihat semua grup dengan kata kunci /lihat_semua
2. Pilih "Pesan Anonim / Mode Rahasia" dan ikuti prosedur hingga selesai

==========

> Membuat & Melihat Profil Anda di grup/organisasi

1. Ketik /buat_profil untuk mengisi data diri dan ikuti proses hingga selesai
2. Ketik /lihat_profil untuk melihat data diri

==========

*Semua fitur di atas dapat digunakan secara penuh jika telah sinkron menjadi anggota minimal 1 \
grup/organisasi menggunakan Orgo

Orgo juga menyediakan fitur penunjang lain seperti terjemahan antarbahasa, pencarian gizi \
pada hidangan, peramalan cuaca, dan kalkulator.
'''

fiturTerjemahan = '''Orgo juga bisa melakukan terjemahan lho!
Fitur ini sangat fleksibel, bisa menerjemahkan bahasa A ke bahasa B, \
bisa pula dari bahasa B ke C, atau bahasa D ke A, dan lainnya 

Cukup dengan bertanya, tetapi AKHIR KALIMAT harus mengandung:
"bahasa" <spasi> nama bahasa tujuan <spasi> kalimat yang ingin diterjemahkan (bahasa apapun)

Contoh:
Apa sih bahasa jepangnya dari saya mau tidur
Hmm bahasa jerman 私は食べたい
Tolong dong kira - kira bahasa koreanya i love you

Bahasa yang didukung:
Arab, Indonesia, Inggris, Italia, Jepang, Jerman, Korea, Perancis, Portugis, Rusia, Spanyol, Swedia, Thailand, Vietnam

Catatan: untuk bahasa kalimat asal yang tidak mendukung huruf latin, disarankan menggunakan aksara aslinya (seperti contoh kedua)

Menarik bukan? Cobalah sekarang juga!'''

fiturCuaca = '''Orgo juga bisa melakukan peramalan cuaca lho!
Cukup dengan kirimkan lokasi anda dengan fitur kirim lokasi, maka cuaca untuk hari ini hingga 2 hari ke depan akan Anda dapatkan!
Menarik bukan? Cobalah sekarang juga!'''

fiturGizi = '''Orgo juga bisa melakukan pencarian gizi lho!
Cukup dengan bertanya, tetapi AKHIR KALIMAT harus mengandung:
kandungan dari <spasi> nama makanan ATAU
gizi dari <spasi> nama makanan

Contoh:
Apa sih gizi dari rendang
Halo kira - kira apa ya kandungan dari soto ayam

Nama makanan saat ini hanya mendukung bahasa indonesia & bahasa inggris
Menarik bukan? Cobalah sekarang juga!'''

fiturKalkulator = '''Orgo juga bisa berhitung lho!
Cukup dengan bertanya, tetapi AKHIR KALIMAT harus mengandung:
hitung <spasi> Perhitungan

Contoh:
Tolong dong hitung 9 ** 2 + 5 / 8
Ano... hitung 90//7

Operasi perhitungan yang didukung:
+ (Tambah), - (Kurang)
/ (Bagi), * (Kali)
// (Fungsi lantai), % (Sisa bagi/Modulo)
**n (Dipangkatkan sebesar n)

Menarik bukan? Cobalah sekarang juga!'''
