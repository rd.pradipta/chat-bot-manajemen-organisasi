import psycopg2

from resources.database.session_db import get_db_session, update_session
from resources.global_var import set_event, set_session, get_database_conn, set_database_conn
from resources.setup import database_url


def per_session_basic_setup(func):
    try:
        conn = psycopg2.connect(database_url)
        set_database_conn(conn)
    except:
        print("failed to connect database")

    def wrapper(event):
        set_event(event)
        conn = get_database_conn()
        try:
            cur = conn.cursor()
            # print("DATABASE CONNECTION : OK")
        except psycopg2.InterfaceError as e:
            conn = psycopg2.connect(database_url)
            cur = conn.cursor()
            set_database_conn(conn)
            print("recovering database...")
        finally:
            cur.close()

        session = get_db_session()
        if session is None:  # user pertamakali ngomong di source ini
            update_session("default")
            session = get_db_session()
        set_session(session)

        message_details = "None"
        if event.type == 'postback':
            message_details = "POSTBACK DATA: " + event.postback.data
        elif event.type == 'message':
            message_details = "MESSAGE TEXT: " + event.message.text

        print("=== === === MESSAGE IN === === ===")
        print("USER_ID: " + event.source.user_id)
        print("SOURCE TYPE: " + event.source.type)
        print("SESSION: " + session)
        print("EVENT TYPE: " + event.type)
        print(message_details)
        print("=== === === === === === === === ===")
        # func(event)
        func()

    return wrapper
