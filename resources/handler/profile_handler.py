import os.path
import sys

import linebot
from linebot.models import SourceGroup, SourceRoom, SourceUser, TextSendMessage

from resources.database.session_db import update_session
from resources.errors.db_errors import UserHasNoProfileError
from resources.handler.sender import reply
from resources.setup import line_bot_api

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")

from resources.help import ERROR_NOT_JOINED_ANY_ORG
from resources.database.user_db import get_user_organizations
from resources.database.user_db import get_user_profile, get_user_position_in_org
from resources.global_var import get_current_event
from resources.modules.user_interface import profil_saya_lengkap, profil_saya_kosong, memilih_org


def create_profile():
    event = get_current_event()
    if isinstance(event.source, SourceUser):
        try:
            organizations = get_user_organizations(event.source.user_id)
            if len(organizations) == 0:
                reply(ERROR_NOT_JOINED_ANY_ORG)
            else:
                msg = "Silakan masukkan data diri Anda dengan format:\n" \
                      + "Token | Nama | Tanggal Lahir | Jabatan (Di Grup Itu) | Skill Anda\n" \
                      + "\n" \
                      + "Contoh:\n" \
                      + "0xffff | Pradipta Gitaya | 24/01/1997 | PIC Compfest XXX | Gaming Hard " \
                        "9/10\n" + "\n" \
                      + "Pilih grup di bawah ini untuk mendapatkan token\n" \
                      + "Ketikkan BATAL untuk membatalkan aksi ini"

                reply(msg)
                ui = memilih_org(event, "jabatan", organizations)
                line_bot_api.reply_message(event.reply_token, ui)
                update_session("buat-profil")

        except linebot.exceptions.LineBotApiError:
            reply(
                "Mohon tambahkan Orgo sebagai teman dulu ya :) Nanti Orgo tidak dapat membalas "
                "pesan")
    else:
        reply("Fitur ini hanya dapat digunakan dalam Personal Chat (PM)\n" +
                  "Silakan coba kembali dengan keyword /buat_profil")


def get_profile():
    event = get_current_event()
    print("Proses melihat profil dimulai")
    user_id = event.source.user_id

    try:

        print("Mencoba mengambil profil pengguna db")
        user_profile = get_user_profile(user_id)
        print("Mengambil profil berhasil, data:\n" + str(user_profile))
        print("Mengambil data jabatan pengguna")
        user_position = get_user_position_in_org(user_id)
        print("Sukses, data = " + str(user_position))

        if len(user_position) != 0:
            print("Pengguna SUDAH mengisi biodata di grup/organisasi, memulai membuat UI")

            try:
                line_bot_api.push_message(event.source.user_id, profil_saya_lengkap(event, user_profile, user_position))
                print("Profil berhasil ditampilkan")
            except linebot.exceptions.LineBotApiError:
                line_bot_api.reply_message(event.reply_token, TextSendMessage(
                    text="Mohon tambahkan Orgo sebagai teman dulu ya :) Nanti Orgo tidak dapat membalas pesan"))
            else:
                if isinstance(event.source, SourceRoom) or isinstance(event.source, SourceGroup):
                    line_bot_api.reply_message(event.reply_token, TextSendMessage(text="Silakan periksa personal chat Anda"))

    except UserHasNoProfileError:
        print("Profil belum mengisi biodata atau belum sync sama sekali, memulai membuat UI khusus")
        if isinstance(event.source, SourceRoom) or isinstance(event.source, SourceGroup):
            try:
                line_bot_api.push_message(event.source.user_id, profil_saya_kosong(event))
                print("Profil berhasil ditampilkan")
            except linebot.exceptions.LineBotApiError:
                line_bot_api.reply_message(event.reply_token, TextSendMessage(
                    text="Mohon tambahkan Orgo sebagai teman dulu ya :) Nanti Orgo tidak dapat membalas pesan"))
            else:
                line_bot_api.reply_message(event.reply_token, TextSendMessage(text="Silakan periksa personal chat Anda"))
