from linebot.models import SourceGroup

from resources.database.organization_db import exist_organization
from resources.database.user_db import user_already_in_organization
from resources.global_var import get_current_event
from resources.handler.sender import reply
from resources.help import ERROR_ORGANIZATION_NOT_REGISTERED, ERROR_NO_ACCESS


# def basic_create_handler(create_format: str):
#     event = get_current_event()
#     user_id = event.source.user_id
#     if isinstance(event.source, SourceGroup):
#         group_id = event.source.group_id
#         if exist_organization(group_id) and user_already_in_organization(user_id, group_id):
#             set_mode(new_mode)
#             reply(create_format)
#         elif not exist_organization(event.source.group_id):
#             reply(ERROR_ORGANIZATION_NOT_REGISTERED)
#         else:
#             reply(ERROR_NO_ACCESS)
#     else:
#         reply(ERROR_NON_GROUP)


def basic_check(func, *args, **kwargs):
    def basic_check_wrapper(*args, **kwargs):
        event = get_current_event()
        user_id = event.source.user_id

        if isinstance(event.source, SourceGroup):
            group_id = event.source.group_id
            if exist_organization(group_id) and user_already_in_organization(user_id, group_id):
                func(*args, **kwargs)
            elif not exist_organization(event.source.group_id):
                reply(ERROR_ORGANIZATION_NOT_REGISTERED)
            else:
                reply(ERROR_NO_ACCESS)
        else:
            func(*args, **kwargs)
    return basic_check_wrapper
