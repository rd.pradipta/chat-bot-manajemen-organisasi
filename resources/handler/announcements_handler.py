import linebot

from resources.database.session_db import update_session, append_session_data, get_session_data, \
    clear_session_data
from resources.handler.basic_handler import basic_check
from resources.handler.sender import reply
from resources.models import Announcement
from resources.setup import line_bot_api

# sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")

from linebot.models import SourceGroup, SourceRoom, TextSendMessage

from resources.help import ERROR_NOT_JOINED_ANY_ORG, ERROR_NON_GROUP
from resources.database.user_db import get_user_organization_ids
from resources.database.announcement_db import get_org_announcements, add_org_announcement
from resources.database.organization_db import get_organization_details, get_members_id
from resources.global_var import get_current_event
from resources.modules.user_interface import lihat_pengumuman_organisasi

CREATE_NEW_ANNOUNCEMENT_FORMAT = (
        "Silakan masukkan detail pengumuman dengan format:\n"
        + "Judul Pengumuman | Deskripsi\n"
        + "\n"
        + "Contoh:\n"
        + "Pengambilan Berkas Ujian Kalkulus III | Mahasiswa diharapkan mengambil di ruangan 2701 "
          "pada pukul 13:00\n"
        + "\n"
        + "Ketikkan BATAL untuk membatalkan aksi ini")

PLEASE_INSERT_ANNOUNCEMENT_TITLE = (
        "Silakan masukkan judul pengumuman baru\n"
        + "\n"
        + "Ketikkan BATAL untuk membatalkan aksi ini")

PLEASE_INSERT_ANNOUNCEMENT_TEXT = (
        "Silakan masukkan isi pengumuman\n"
        + "\n"
        + "Ketikkan BATAL untuk membatalkan aksi ini")


@basic_check
def create_announcement():
    event = get_current_event()
    if isinstance(event.source, SourceGroup):
        reply(PLEASE_INSERT_ANNOUNCEMENT_TITLE)
        update_session("create-announcement-stage-1")
    else:
        reply(ERROR_NON_GROUP)


def create_announcement_stage_1(announcement_title):
    append_session_data(announcement_title)
    reply(PLEASE_INSERT_ANNOUNCEMENT_TEXT)
    update_session("create-announcement-stage-2")

def create_announcement_stage_2(announcement_text):
    group_id = get_current_event().source.group_id
    announcement_title = get_session_data()[0]
    announcement = Announcement(id=-1,
                                title=announcement_title,
                                text=announcement_text)
    result = add_org_announcement(announcement_tuple=announcement,
                                  organization_id=group_id)
    reply(result)
    notify_new_announcement(announcement)
    clear_session_data()
    update_session("default")



@basic_check
def get_announcement(group_id=None):
    event = get_current_event()

    if group_id is not None:
        announcements = get_org_announcements(group_id)
        lihat_pengumuman_organisasi(announcements, show_edit=False)

    elif isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
        announcements = get_org_announcements(group_id)
        lihat_pengumuman_organisasi(announcements)

    else:
        try:
            user_id = event.source.user_id
            organization_ids = get_user_organization_ids(user_id)
            if len(organization_ids) == 0:
                reply(ERROR_NOT_JOINED_ANY_ORG)
            elif len(organization_ids) > 20:
                reply("Mohon maaf, jumlah organisasi Anda melebihi batas (20)")
            else:
                announcements = []
                for organization_id in organization_ids:
                    announcements.extend(get_org_announcements(organization_id))
                lihat_pengumuman_organisasi(announcements, show_edit=False)
                if isinstance(event.source, SourceRoom):
                    reply("Silakan cek personal chat Anda")
        except linebot.exceptions.LineBotApiError:
            reply(
                "Mohon tambahkan Orgo sebagai teman dulu ya :) Nanti Orgo tidak dapat membalas pesan")


def notify_new_announcement(announcement: Announcement):
    event = get_current_event()
    group_id = event.source.group_id
    organization_name = get_organization_details(group_id).name

    schedule_string = ("Ada Pengumuman Baru!" + "\n"
                       + "\n"
                       + "Organisasi : " + organization_name + "\n"
                       + "Judul : " + announcement.title + "\n"
                       + "Isi : " + announcement.text + "\n"
                       + "\n"
                       + "Untuk lebih detailnya, bisa dilihat dengan mengetik /lihat_pengumuman"
                       )

    members_id = get_members_id(group_id)
    for member_id in members_id:
        line_bot_api.push_message(member_id, TextSendMessage(text=schedule_string))
