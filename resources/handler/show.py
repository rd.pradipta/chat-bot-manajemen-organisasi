from linebot.models import SourceGroup

from resources.global_var import get_current_event
from resources.handler.sender import reply
from resources.modules.user_interface import get_menu_grup, get_menu_nongrup


def show_menu():
    event = get_current_event()

    # jika chat bersumber dari grup
    if isinstance(event.source, SourceGroup):
        ui = get_menu_grup(event)
        reply(ui)

    # jika chat tidak bersumber darii grup
    else:
        ui = get_menu_nongrup(event)
        reply(ui)
