import linebot
from linebot.models import SourceGroup, SourceRoom, TextSendMessage, FlexSendMessage

from resources.database.organization_db import exist_organization
from resources.database.session_db import update_session, append_session_data, get_session_data
from resources.database.user_db import add_new_member, get_user_organizations
from resources.global_var import get_current_event
from resources.handler.sender import reply
from resources.modules.user_interface import pembuat_organisasi, \
    group_column_ui
from resources.help import ERROR_NON_GROUP, ERROR_NOT_JOINED_ANY_ORG
from resources.setup import line_bot_api

PLEASE_INSERT_ORGANIZATION_NAME = (
        "Silakan masukkan nama grup atau organisasi\n"
        + "\n"
        + "Nama bisa berbeda dengan nama grup yang ditampilkan pada line\n"
        + "\n"
        + "Ketikkan BATAL untuk membatalkan aksi ini"
)

PLEASE_INSERT_ORGANIZATION_DESCRIPTIONS = (
        "Silakan masukkan deskripsi organisasi\n"
        + "\n"
        + "Ketikkan BATAL untuk membatalkan aksi ini"
)

GROUP_ALREADY_REGISTERED_MESSAGE = (
        "Mohon maaf, Grup ini telah terdaftar di data Orgo. 1 Grup hanya perlu didaftarkan sekali "
        "saja\n "
        + '\n'
        + "Silakan tekan DAFTAR & SINKRON di menu HELP atau ketik /sync (Dengan tanda /) untuk "
          "mendaftar sebagai anggota & sinkron semua informasi grup/organisasi ini ke profil Anda"
)


def create_organization():
    event = get_current_event()
    if isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
        if not exist_organization(group_id):
            reply(PLEASE_INSERT_ORGANIZATION_NAME)
            update_session("create-organization-stage-1")
        else:
            reply(GROUP_ALREADY_REGISTERED_MESSAGE)
    else:
        reply(ERROR_NON_GROUP)


def create_organization_stage_1(organization_name: str):
    append_session_data(organization_name)
    reply(PLEASE_INSERT_ORGANIZATION_DESCRIPTIONS)
    update_session("create-organization-stage-2")


def create_organization_stage_2(organization_descriptions: str):
    event = get_current_event()

    session_data = get_session_data()
    session_data.append(organization_descriptions)
    organization_detail = session_data

    append_session_data(organization_descriptions)

    organization_creation_confirmation_ui = pembuat_organisasi(organization_detail)
    showUI = FlexSendMessage(alt_text="Konfirmasi Grup/Organisasi",
                             contents=organization_creation_confirmation_ui)

    line_bot_api.reply_message(event.reply_token, showUI)
    update_session("create-organization-stage-3")


def sync_user():
    event = get_current_event()

    if isinstance(event.source, SourceGroup):
        proses_daftar = add_new_member(event.source.group_id, event.source.user_id)
        reply(proses_daftar)
    else:
        reply(ERROR_NON_GROUP)


def get_all():
    event = get_current_event()

    organizations = get_user_organizations(event.source.user_id)
    profile = line_bot_api.get_profile(event.source.user_id)
    if len(organizations) == 0:
        reply(ERROR_NOT_JOINED_ANY_ORG)
    elif len(organizations) > 20:
        reply("Mohon maaf, jumlah organisasi Anda melebihi batas (20)")
    else:
        ui = group_column_ui(organizations)
        try:
            reply([TextSendMessage(text="Halo "
                                   + profile.display_name
                                   + ", berikut adalah daftar grup/organisasi yang Anda ikuti"),
                   ui]
                  )
        except linebot.exceptions.LineBotApiError:
            reply("Mohon tambahkan Orgo sebagai teman dulu ya :) Nanti Orgo tidak dapat membalas pesan")
        else:
            if isinstance(event.source, SourceGroup) or isinstance(event.source, SourceRoom):
                reply("Silakan periksa personal chat Anda")
