import linebot
import sys
import os.path

from resources.database.session_db import update_session
from resources.handler.basic_handler import basic_check
from resources.handler.sender import reply
from resources.setup import line_bot_api

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")

from linebot.models import SourceGroup, SourceRoom

from resources.help import ERROR_NOT_JOINED_ANY_ORG, ERROR_NON_GROUP
from resources.database.user_db import get_user_organizations
from resources.database.workplans_db import get_org_workplans
from resources.global_var import get_current_event
from resources.modules.user_interface import memilih_org, brief_workplan_ui

CREATE_NEW_WORKPLAN_FORMAT = (
        "Silakan masukkan detail proker dengan format:\n" +
        "Nama Workplan | Penanggung Jawab | Deskripsi\n" + "\n" +
        "Contoh:\n" +
        "Pembuatan Chat Bot Organisasi | Tim Maba Palsu Compfest X | Dengan "
        "hadirnya bot ini, diharapkan segala bentuk kegiatan organisasi/grup dapat dikontrol dan "
        "dikerjakan lebih mudah. Workplan ini bersifat sangat penting dan proyeknya bersifat "
        "sangat rahasia \n" + "\n" +
        "Ketikkan BATAL untuk membatalkan aksi ini")


@basic_check
def create_workplan():
    if isinstance(event.source, SourceGroup):
        update_session("buat-proker")
        reply(CREATE_NEW_WORKPLAN_FORMAT)
    else:
        reply(ERROR_NON_GROUP)


@basic_check
def get_workplan(group_id=None):
    event = get_current_event()

    if group_id is not None:
        workplans = get_org_workplans(group_id)
        # organization_name = get_organization_details(group_id).name
        # lihat_proker_organisasi(event, workplans, organization_name)
        if workplans == []:
            reply("Organisasi belum memiliki Program Kerja. Silakan tambahkan terlebih dahulu")
            return
        ui = brief_workplan_ui(workplans)
        reply(ui)

    elif isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
        workplans = get_org_workplans(group_id)
        # organization_name = get_organization_details(group_id).name
        # lihat_proker_organisasi(event, workplans, organization_name)
        if workplans == []:
            reply("Organisasi belum memiliki Program Kerja. Silakan tambahkan terlebih dahulu")
            return
        ui = brief_workplan_ui(workplans)
        reply(ui)


    else:
        try:
            user_id = event.source.user_id
            organizations = get_user_organizations(user_id)
            if len(organizations) == 0:
                reply(ERROR_NOT_JOINED_ANY_ORG)
            elif len(organizations) > 20:
                reply("Mohon maaf, jumlah organisasi Anda melebihi batas (20)")
            else:
                ui = memilih_org(event,"proker",organizations)
                reply(ui)
                if isinstance(event.source, SourceRoom):
                    reply("Silakan cek personal chat Anda")
        except linebot.exceptions.LineBotApiError:
            reply("Mohon tambahkan Orgo sebagai teman dulu ya :) Nanti Orgo tidak dapat membalas pesan")



# @basic_check()
def show_brief_workplans():
    event = get_current_event()

    if isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
        workplans = get_org_workplans(group_id)
        # organization_name = get_organization_details(group_id).name
        ui = brief_workplan_ui(workplans)
        line_bot_api.reply_message(event.reply_token, ui)

    else:
        reply(ERROR_NON_GROUP)
