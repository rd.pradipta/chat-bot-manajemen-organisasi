# coding=utf-8
"""
Utility untuk database
"""


def normalize_sql_varchar(string, varchar_name="Eror"):
    """
    menomalisasi string sql yang aneh
    :param string: string sql
    :param varchar_name: kategori dari string
    :return string: string yang sudah di normalisasi
    """
    if string is None:
        return varchar_name + " tidak ada"
    if string[0] != "{":
        return string
    string = string[1:len(string) - 1]
    string = " ".join(string.split(","))
    return string


def normalize_sql_tuple(sql_tuple: tuple):
    """
    Normalize weird tuple given by sql
    :param sql_tuple: wird tuple
    :return: proper tuple
    """
    sql_tuple_string = sql_tuple[0]
    sql_tuple_string = sql_tuple_string[1:len(sql_tuple_string) - 1]
    splitted_tuple = sql_tuple_string.split(",")
    proper_list = []
    for i in range(len(splitted_tuple)):
        element = splitted_tuple[i]
        if element == "":
            proper_element = " "
        elif element[0] == '"':
            proper_element = element[1:len(element) - 1]
        else:
            proper_element = element
        proper_list.append(proper_element)
    return proper_list


def beautify_db_datetime_format(datetime: str):
    print(datetime)
    date = datetime.split("T")[0]
    tahun = date.split("-")[0]
    bulan = date.split("-")[1]
    tanggal = date.split("-")[2]
    time = datetime.split("T")[1]

    def get_bulan(bulan: str):
        if bulan == "01":
            return "Januari"
        elif bulan == "02":
            return "Februari"
        elif bulan == "03":
            return "Maret"
        elif bulan == "04":
            return "April"
        elif bulan == "05":
            return "Mei"
        elif bulan == "06":
            return "Juni"
        elif bulan == "07":
            return "Juli"
        elif bulan == "08":
            return "Agustus"
        elif bulan == "09":
            return "September"
        elif bulan == "10":
            return "Oktober"
        elif bulan == "11":
            return "November"
        elif bulan == "12":
            return "Desember"

    datetime = tanggal + " " + get_bulan(bulan) + " " + tahun + " Pukul " + time
    return datetime
