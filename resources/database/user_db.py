# coding=utf-8
"""
Interface untuk mengakses database pengguna
"""
import psycopg2

from resources.database.organization_db import exist_organization, get_organization, \
    get_organization_details
from resources.database.utilities import normalize_sql_varchar, normalize_sql_tuple
from resources.errors.db_errors import UserHasNoProfileError
from resources.global_var import get_database_conn
from resources.models import User
from resources.setup import line_bot_api, database_url

ERROR_ORGANIZATION_NOT_REGISTERED = \
    '''Grup/organisasi belum terdaftar di Orgo.

Cara mendaftar:
1. Undang Orgo ke Grup Anda
2. Masuk ke Menu dengan mengetik /menu pada obrolan grup (Jangan lupa tanda /)
3. Tekan TAMBAH ORG./GRUP
4. Ikuti Prosesnya hingga selesai

Catatan:
Setelah berhasil, semua anggota grup diwajibkan menekan DAFTAR & SINKRON
Hal ini agar setiap orang terdata oleh Orgo sebagai anggota grup
Selesai, mudah bukan!'''


def exist_user(user_id):
    """
    Mengecek apakah pengguna sudah ada di database
    :param user_id: id pengguna
    :return boolean: ada tidaknya pengguna
    """

    conn = None
    is_exist = True

    # print("\nStart checking user existence...")
    try:
        conn = psycopg2.connect(database_url)
        cur = conn.cursor()

        # print("Checking user existence from table users")
        cur.execute(
            """
            SELECT user_name
            FROM users
            WHERE user_id = %s
            """, (user_id,))

        row = cur.fetchone()

        if row is None:
            is_exist = False

        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found Error")
        # print(error)
        return True
    finally:
        if conn is not None:
            # print("Closing connection")
            conn.close()
        # print("End Checking user existance\n")

    return is_exist


def user_already_in_organization(user_id, organization_id):
    """
    Mengecek apakah pengguna sudah terdaftar di sebuah organisasi
    :param user_id: id pengguna
    :param organization_id: id organisasi
    :return boolean: ada atau tidaknya pengguna dalam organisasi
    """
    conn = get_database_conn()
    is_exist = True

    # print("\nStart checking if user already registered in organization...")
    try:
        # conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        # print("Checking user existence in organization on table organization_members")
        cur.execute(
            """
            SELECT user_id
            FROM organization_members
            WHERE user_id = %s AND organization_id = %s
            """, (user_id, organization_id,))

        row = cur.fetchone()

        if row is None:
            is_exist = False

        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found Error")
        # print(error)
        return True
    # finally:
    #     if conn is not None:
    #         # print("Closing connection")
            # conn.close()
        # print("aggota is already exist in organisasi : " + str(is_exist))

    return is_exist


def add_new_member(organization_id: str, user_id: str):
    """
    Menambahkan pengguna sebagai anggota baru pada organisasi
    :param organization_id: id organisasi
    :param user_id: id pengguna
    :return:
    """

    if not exist_organization(organization_id):
        # print("organization not registered")
        return (ERROR_ORGANIZATION_NOT_REGISTERED)

    if user_already_in_organization(user_id, organization_id):
        # print("user already registered in this organization")
        return "Mohon maaf, Anda telah terdaftar & tersinkron dengan Orgo di grup/organisasi ini"

    user_name = line_bot_api.get_group_member_profile(organization_id, user_id).display_name

    # print("\nStart inserting member...")

    # conn = None
    conn = get_database_conn()

    try:
        # conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        if not exist_user(user_id):
            # print("inserting user data to users table")
            cur.execute(
                """
                INSERT INTO users (user_id, user_name)
                VALUES (%s,%s)
                """, (user_id, user_name,))
            # print("new user registered")

        # else:
            # print("user is not new")

        # print("inserting user data to organization_users table ...")
        cur.execute(
            """
            INSERT INTO organization_members (organization_id, user_id)
            VALUES (%s,%s)
            """, (organization_id, user_id,))

        conn.commit()

        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found error")
        # print(error)
        return "Mohon maaf, terjadi kesalahan sistem saat Orgo hendak mendaftarkan Anda! " \
               "Cobalah kembali sesaat lagi "

    # finally:
    #     if conn is not None:
            # print("Closing connection...")
            # conn.close()
    # print("End inserting user...")

    return "Terima Kasih. Anda telah tersinkron dengan Orgo"


def get_user_organization_ids(user_id):
    """
    Mendapatkan id organisasi2 yang didaftarkan pengguna
    :param user_id: id pengguna
    :return: list of organization id
    """
    organizations_id = []

    # conn = None
    conn = get_database_conn()
    # print("\nStart getting user organization ids...")
    try:
        # conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        # print("Getting user organizations ids from table organization_members")
        cur.execute(
            """
            SELECT organization_id
            FROM organization_members
            WHERE user_id = %s
            """, (user_id,))

        # cur.fetchone()

        rows = cur.fetchall()

        for row in rows:
            organization_id = row[0]
            organizations_id.append(organization_id)

        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found Error")
        # print(error)
        return True

    # finally:
    #     if conn is not None:
    #         # print("Closing connection")
    #         # conn.close()
    #     # print("User organizations :")
    #     for organization_id in organizations_id:
    #         # print("---" + organization_id)
    #     # print("End getting user organizations id")

    return organizations_id


def get_user_organizations(user_id: str):
    """
    Mendapatkan semua organisasi yang didaftarkan pengguna
    :param user_id: iid pengguna
    :return: list dari named-tuple organisasi
    """
    organization_ids = get_user_organization_ids(user_id)

    organizations = []

    for organization_id in organization_ids:
        organization = get_organization(organization_id)
        organizations.append(organization)

    return organizations


def modify_user_profile(user_data: User):
    """
    Mengubah profile pengguna, menambahkan jika belum ada.
    :param user_data: data pengguna
    :return: berhasil atau tidaknya pengubahan profil pengguna
    """

    # conn = None
    conn = get_database_conn()

    # # print("Start modifying user profile with user name : " + user_data.name)

    try:
        # conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        # print("modifying user profile in users table ...")
        cur.execute(
            """
            UPDATE users
            SET user_name = %s,
                user_bod = %s,
                user_skill = %s
            WHERE user_id = %s
            """, (user_data.name,
                  user_data.bod,
                  user_data.skill,
                  user_data.id))

        conn.commit()
        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found error")
        # print(error)
        return "Terjadi kesalahan saat Orgo hendak menambah/mengubah profil pengguna, silakan coba kembali" \
               " beberapa saat lagi"

    # finally:
    #     if conn is not None:
            # print("Closing connection...")
            # conn.close()

    # print("End modifying user profile...")

    return "Terima kasih, profil Anda berhasil ditambah/diubah"


def get_user_profile(user_id: str):
    """
    Mendapatkan profil pengguna
    :param user_id: id pengguna
    :return: named-tuple data pengguna
    """

    # conn = None

    conn = get_database_conn()

    # print("\nStart getting profile of user : " + user_id + " ...")
    try:

        # conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        # print("Getting user profile from users table")
        cur.execute(
            """
            SELECT (user_name, user_bod, user_skill)
            FROM users
            WHERE user_id = %s
            """, (user_id,))

        row = cur.fetchone()
        if row is None:
            raise UserHasNoProfileError
        row = normalize_sql_tuple(row)
        user_name = normalize_sql_varchar(row[0])
        user_bod = normalize_sql_varchar(row[1])
        user_skill = normalize_sql_varchar(row[2])

        user_profile = User(id=user_id, name=user_name, bod=user_bod, skill=user_skill)

        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found Error")
        # print(error)
        return None

    # finally:
    #     if conn is not None:
    #         # print("Closing connection")
            # conn.close()
        # print("End getting user profile")

    return user_profile


def get_user_position_in_org(user_id):
    """
    Mendapatkan semua jabatan pengguna pada organisasi
    :param user_id: id user yang jabatannya akan di dapatkan
    :return: list of list, yang mana setiap list nya berisi ("nama organisasi","jabatan")
    """
    user_positions = []
    # conn = None

    conn = get_database_conn()
    # print("Start getting user:" + user_id + " positions ...")

    try:
        # conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        # print("Getting user positions from organization_members table ...")
        cur.execute(
            """
            SELECT organization_id, user_position
            FROM organization_members
            WHERE user_id = %s
            """, (user_id,))

        # cur.fetchone()

        rows = cur.fetchall()
        for row in rows:
            organization_id = row[0]
            organization_name = get_organization_details(organization_id).name
            position = normalize_sql_varchar(row[1])
            user_positions.append([organization_name, position])

        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found Error")
        # print(error)
        return True

    # finally:
    #     if conn is not None:
            # print("Closing connection")
            # conn.close()
        # print("End getting user positions")

    return user_positions


def modify_user_position_in_org(user_id, organization_id, user_position: str):
    """
    Mengubah jabatan pengguna. Menambahkan jabatan jika jabatan belum terdaftar.
    :param user_id: id pengguna yang akan diubah jabatannya
    :param organization_id: id organisasi yang akan diubah jabatan user nya
    :param user_position: jabatan baru pengguna
    :return: laporan berhasil atau tidaknya pengubahan jabatan
    """
    # conn = None

    conn = get_database_conn()
    # print("Start adding user:" + user_id
    #       + " position as " + user_position
    #       + " in organization:" + organization_id + " ...")

    try:
        # conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        # print("Modifying user position in organization_members table ...")
        cur.execute(
            """
            UPDATE organization_members
            SET user_position = %s
            WHERE user_id = %s
            AND organization_id = %s
            """, (user_position,
                  user_id,
                  organization_id))

        conn.commit()
        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found error")
        # print(error)
        return "Terjadi kesalahan saat Orgo hendak mengubah jabatan Anda, silakan coba " \
               "kembali beberapa saat lagi "

    # finally:
    #     if conn is not None:
    #         # print("Closing connection...")
            # conn.close()

    # print("End modifying user position...")

    return "OK"


def get_all_users_id():
    conn = get_database_conn()
    cur = conn.cursor()

    users_id = []
    cur.execute(
        """
        SELECT user_id
        FROM users
        """
    )
    rows = cur.fetchall()
    cur.close()
    if rows is not None:
        for row in rows:
            user_id = row[0]
            users_id.append(user_id)
    return users_id


if __name__ == '__main__':
    # test purpose
    import os

    aab = User(name="aabccd", bod="feb", skill="japanese", id=3)
    user_id = os.environ["USER"]
    group_id = os.environ["GRUP"]
    # # print(modify_user_position_in_org(user_id, group_id, "shacho"))
    positions = get_user_position_in_org(user_id)
    # for pos in positions:
        # print(pos)
