import psycopg2

from linebot.models import SourceGroup, SourceUser, SourceRoom

from resources.global_var import get_database_conn, get_current_event, set_database_conn, \
    get_current_session
from resources.setup import database_url


def get_db_session():
    try:
        event = get_current_event()
        conn = get_database_conn()

        cur = conn.cursor()
        user_id = event.source.user_id
        group_id = "private_chat"
        if isinstance(event.source, SourceGroup):
            group_id = event.source.group_id

        cur.execute(
            """
            SELECT session_name
            FROM sessions
            WHERE organization_id = %s AND user_id = %s
            """, (group_id, user_id)
        )

        session = cur.fetchone()

        if session:
            session = session[0]
        cur.close()
    except psycopg2.InternalError as e:
        print(e)
        set_database_conn(database_url)
        session = get_db_session()

    return session


def update_session(session_type):

    session_data = get_session_data()
    if session_data == []:
        session_data = None
    else:
        session_data = "&&&&".join(session_data)

    event = get_current_event()
    conn = get_database_conn()

    cur = conn.cursor()

    user_id = event.source.user_id
    if isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
    elif isinstance(event.source, SourceRoom):
        group_id = "room"
    elif isinstance(event.source, SourceUser):
        group_id = "private_chat"


    delete_session()
    cur.execute(
        """
        INSERT INTO sessions (session_name,
                              user_id,
                              organization_id,
                              session_data)
        VALUES (%s, %s, %s, %s)
        """, (session_type,
              user_id,
              group_id,
              session_data)
    )
    conn.commit()
    cur.close()



def get_session_data():

    event = get_current_event()
    conn = get_database_conn()

    cur = conn.cursor()

    user_id = event.source.user_id
    if isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
    elif isinstance(event.source, SourceRoom):
        group_id = "room"
    elif isinstance(event.source, SourceUser):
        group_id = "private_chat"


    cur.execute(
        """
        SELECT session_data
        FROM sessions
        WHERE user_id = %s AND organization_id = %s
        """, (user_id,
              group_id)
    )
    session_data = cur.fetchone()
    if session_data is None:
        session_data = [None]
    session_data = session_data[0]
    if session_data is None:
        session_data = []
    else:
        session_data = session_data.split("&&&&")

    session_data
    cur.close()
    return session_data

def append_session_data(data: str):

    session_type = get_current_session()
    session_data = get_session_data()
    session_data.append(data)
    session_data = "&&&&".join(session_data)

    event = get_current_event()
    conn = get_database_conn()

    cur = conn.cursor()

    user_id = event.source.user_id
    if isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
    elif isinstance(event.source, SourceRoom):
        group_id = "room"
    elif isinstance(event.source, SourceUser):
        group_id = "private_chat"


    delete_session()
    cur.execute(
        """
        INSERT INTO sessions (session_name,
                              user_id,
                              organization_id,
                              session_data)
        VALUES (%s, %s, %s, %s)
        """, (session_type,
              user_id,
              group_id,
              session_data)
    )
    conn.commit()
    cur.close()

def clear_session_data():
    session_type = get_current_session()

    event = get_current_event()
    conn = get_database_conn()

    cur = conn.cursor()

    user_id = event.source.user_id
    if isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
    elif isinstance(event.source, SourceRoom):
        group_id = "room"
    elif isinstance(event.source, SourceUser):
        group_id = "private_chat"


    delete_session()
    cur.execute(
        """
        INSERT INTO sessions (session_name,
                              user_id,
                              organization_id,
                              session_data)
        VALUES (%s, %s, %s, %s)
        """, (session_type,
              user_id,
              group_id,
              None)
    )
    conn.commit()
    cur.close()


def delete_session():
    event = get_current_event()
    conn = get_database_conn()

    cur = conn.cursor()

    user_id = event.source.user_id
    group_id = "private_chat"
    if isinstance(event.source, SourceGroup):
        group_id = event.source.group_id


    cur.execute(
        """
        DELETE FROM sessions 
        WHERE user_id=%s AND organization_id=%s
        """, (user_id,
              group_id)
    )
    conn.commit()
    cur.close()


# if __name__ == '__main__':
    # user_id = "Uc3a6d0ad902a5b68e296526f7dcd1e96"
    # # session = Session(user_id=user_id,
    # #                   organization_id="Cd5779b7053119966e32b540fb99ebe02",
    # #                   session_type="create-organization")
    # # set_database_conn(psycopg2.connect(DATABASE_URL))
    # # update_session(session)
    # get_db_session()
