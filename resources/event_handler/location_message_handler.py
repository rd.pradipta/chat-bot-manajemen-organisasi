import linebot
from linebot.models import TextSendMessage

from resources.database.schedule_db import update_schedule_db
from resources.database.session_db import update_session
from resources.global_var import get_current_event, get_current_session
from resources.handler.schedule_handler import create_schedule_stage_3
from resources.handler.sender import reply
from resources.modules.fitur_cuaca import get_cuaca
from resources.setup import line_bot_api


def handle_loc_message():
    event = get_current_event()
    session = get_current_session()
    if session == "default":
        try:
            get_cuaca(event)
        except linebot.exceptions.LineBotApiError:
            line_bot_api.reply_message(event.reply_token, TextSendMessage(
                text="Pencarian cuaca gagal!, cobalah untuk mengganti lokasi & periksalah kembali "
                     "koneksi Anda!"))

    elif session == "create-schedule-stage-3":
        schedule_location = event.message.address
        create_schedule_stage_3(schedule_location)

    elif "-".join(session.split("-")[0:3]) == "edit-schedule-location":
        schedule_id = session.split("-")[3]
        schedule_location = event.message.address
        update_schedule_db("location", schedule_location, schedule_id)
        reply("Lokasi jadwal telah diperbarui")
        update_session("default")

