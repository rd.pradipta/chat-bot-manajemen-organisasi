# Import untuk organisasi, jadwal, pengumuman & proker
import linebot
from linebot.models import SourceUser, SourceGroup, SourceRoom, TextSendMessage

import sys
import os.path

from resources.handler.announcements_handler import get_announcement, create_announcement_stage_1, \
    create_announcement_stage_2, notify_new_announcement, create_announcement
from resources.handler.organization_handler import get_all, create_organization_stage_1, \
    create_organization_stage_2, sync_user, create_organization
from resources.handler.profile_handler import get_profile, create_profile
from resources.handler.schedule_handler import get_schedule, create_schedule_stage_1, \
    create_schedule_stage_2, create_schedule_stage_3, create_schedule
from resources.handler.sender import send_message_to_anonymous_member, reply, \
    send_message_to_developer
from resources.handler.show import show_menu
from resources.handler.workplan_handler import get_workplan, create_workplan

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")

from resources.database.announcement_db import add_org_announcement, update_announcement_db
from resources.database.organization_db import get_anonymous_mode, get_anonymous_members_id
from resources.database.schedule_db import update_schedule_db
from resources.database.session_db import update_session, clear_session_data
from resources.database.user_db import modify_user_profile, get_user_organization_ids, \
    modify_user_position_in_org
from resources.database.workplans_db import add_org_workplan, update_workplan_db
from resources.event_handler.file_handler import UPLOAD_NEW_FILE_FORMAT, get_document
from resources.global_var import get_current_event, get_current_session
from resources.models import Announcement, Workplan, User

# Import module untuk fitur pencarian kalori makanan
from resources.modules.fitur_makanan import get_makanan
# Import module untuk fitur terjemahan
from resources.modules.fitur_terjemahan import getTerjemahan
# Import module untuk kalkulator
from resources.modules.fitur_kalkulator import kalkulator

# Import untuk user interface (UI)
from resources.modules.user_interface import menu_bar, get_bantuan, pembuat_pengumuman, \
    pembuat_proker, \
    pembuat_profil_nongrup
# Import untuk komponen utama dan pusat bantuan
from resources.help import ERROR_WRONG_FORMAT, BANTUAN_DASAR
from resources.setup import line_bot_api

keyword_terjemahan = ['apa bahasa', 'apa ya bahasa', 'apa ya bahasanya', 'apa sih bahasa',
                      'apa sih bahasanya']

keyword_gizi = ['apa gizi dari', 'apa kandungan dari', 'bagaimana gizi dari',
                'bagaimana kandungan dari', 'apa sih gizi dari',
                'apa sih kandungan dari', 'gizi dari', 'kandungan dari',
                'apa saja kandungan dari', 'apa saja gizi dari', 'apa aja kandungan dari',
                'apa ya kandungan dari', 'apa sih kandungan dari']


def handle_text_message():
    session = get_current_session()

    event = get_current_event()
    user_id = event.source.user_id
    if isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
    elif isinstance(event.source, SourceUser):
        group_id = "private chat"
    elif isinstance(event.source, SourceRoom):
        group_id = "multi chat"

    masukan_raw = event.message.text
    if masukan_raw is None:
        masukan_raw = ""
    masukan = masukan_raw.lower()
    masukan_underline_splitted = masukan.split('_')
    masukan_space_splitted = masukan.split(' ')
    masukan_raw_bar_splitted = masukan_raw.split(" | ")
    masukan_raw_bar_split_without_space = masukan_raw.split("|")
    komando = masukan_space_splitted[0]

    anonymous_mode_is_on = get_anonymous_mode(group_id)
    if anonymous_mode_is_on:
        try:
            send_message_to_anonymous_member(group_id)
        except linebot.exceptions.LineBotApiError:
            reply(
                "Mohon tambahkan Orgo sebagai teman dulu ya :) Nanti Orgo tidak dapat membalas "
                "pesan")

    if masukan == "/reset":
        update_session("default")
        reply("Orgo berhasil di-reset. Anda dapat menggunakan Orgo kembali")
        return

    elif session == "default":

        if event.source.type == "user":
            menu_bar(user_id)  # membuat rich menu

        if masukan == "test" or masukan == "tes" or masukan == "halo" or masukan == "hai" or masukan == "oi" or masukan == "hoi":
            reply(BANTUAN_DASAR)

        elif masukan == "help" or masukan == "bantuan" or masukan == "/help" or masukan == "/bantuan":
            get_bantuan(event)  # Fungsi ada di file user_interface.py

        elif masukan == "menu" or masukan == "/menu":
            show_menu()

        elif masukan == "pengaturan":
            pass

        elif masukan == "/sync":
            sync_user()

        elif masukan_underline_splitted[0] == "/buat":

            if masukan_underline_splitted[1] == "grup":
                create_organization()

            elif masukan_underline_splitted[1] == "jadwal":
                create_schedule()

            elif masukan_underline_splitted[1] == "pengumuman":
                create_announcement()

            elif masukan_underline_splitted[1] == "proker":
                create_workplan()

            elif masukan_underline_splitted[1] == "profil":
                create_profile()

            elif masukan_underline_splitted[1] == "dokumen":
                reply(UPLOAD_NEW_FILE_FORMAT)

        elif masukan_underline_splitted[0] == "/lihat":

            if masukan_underline_splitted[1] == "jadwal":
                get_schedule()

            elif masukan_underline_splitted[1] == "pengumuman":
                get_announcement()

            elif masukan_underline_splitted[1] == "proker":
                get_workplan()

            elif masukan_underline_splitted[1] == "profil":
                get_profile()

            elif masukan_underline_splitted[1] == "dokumen":
                get_document()

            elif masukan_underline_splitted[1] == "semua":
                get_all()

        elif any(i in masukan for i in keyword_terjemahan) or (
                'bahasa' in masukan and len(masukan_space_splitted) > 2):
            print("Mulai menerjemahkan")
            getTerjemahan(event, masukan)

        elif any(i in masukan for i in keyword_gizi) or (
                'gizi dari' in masukan or 'kandungan dari' in masukan):
            idx = masukan_space_splitted.index('dari')
            makanan_diminta = ' '.join(masukan_space_splitted[idx + 1:])
            print("Mulai mencari data")
            get_makanan(event, makanan_diminta)

        elif komando == "/kirim_pesan":
            saran = ' '.join(masukan_space_splitted[1:])
            send_message_to_developer(saran)

        elif 'hitung' in masukan:
            idx = masukan_space_splitted.index('hitung')
            hitungan_diminta = ' '.join(masukan_space_splitted[idx + 1:])
            print("Mulai mencoba menghitung")
            print("HITUNGLAH: " + hitungan_diminta)
            mulai_hitung = kalkulator(hitungan_diminta)
            reply(mulai_hitung)

        else:
            pass

    elif masukan == 'batal':
        reply("Proses telah dibatalkan")
        clear_session_data()
        update_session("default")

    elif "-".join(session.split("-")[0:2]) == "anonymous-mode":
        group_id = session.split("-")[2]
        message = "Pesan dari user rahasia : \n" + masukan_raw
        line_bot_api.push_message(group_id, TextSendMessage(text=message))
        anonymous_members_id = get_anonymous_members_id(group_id)
        anonymous_members_id.remove(user_id)
        for anonymous_member_id in anonymous_members_id:
            line_bot_api.push_message(anonymous_member_id, TextSendMessage(text=message))

    elif session == "create-organization-stage-1":
        organization_name = masukan_raw
        create_organization_stage_1(organization_name)

    elif session == "create-organization-stage-2":
        organization_descriptions = masukan_raw
        create_organization_stage_2(organization_descriptions)

    elif session == "create-schedule-stage-1":
        schedule_name = masukan_raw
        create_schedule_stage_1(schedule_name)

    elif session == "create-schedule-stage-2":
        schedule_descriptions = masukan_raw
        create_schedule_stage_2(schedule_descriptions)

    elif session == "create-schedule-stage-3":
        schedule_location = masukan_raw
        create_schedule_stage_3(schedule_location)

    elif session == "create-announcement-stage-1":
        announcement_title = masukan_raw
        create_announcement_stage_1(announcement_title)

    elif session == "create-announcement-stage-2":
        announcement_text = masukan_raw
        create_announcement_stage_2(announcement_text)

    elif "-".join(session.split("-")[0:2]) == "edit-schedule":
        attribute = session.split("-")[2]
        schedule_id = session.split("-")[3]

        if attribute == "title":
            update_schedule_db("title", masukan_raw, schedule_id)
            reply("Judul jadwal telah diperbarui")
            update_session("default")

        elif attribute == "location":
            update_schedule_db("location", masukan_raw, schedule_id)
            reply("Lokasi jadwal telah diperbarui")
            update_session("default")

        elif attribute == "date":
            pass

        elif attribute == "descriptions":
            update_schedule_db("descriptions", masukan_raw, schedule_id)
            reply("Deskripsi jadwal telah diperbarui")
            update_session("default")

    elif "-".join(session.split("-")[0:2]) == "edit-announcement":
        attribute = session.split("-")[2]
        announcement_id = session.split("-")[3]

        if attribute == "title":
            update_announcement_db("title", masukan_raw, announcement_id)
            reply("Judul pengumuman telah diperbarui")
            update_session("default")

        elif attribute == "text":
            update_announcement_db("text", masukan_raw, announcement_id)
            reply("Deskripsi pengumuman telah diperbarui")
            update_session("default")

    elif "-".join(session.split("-")[0:2]) == "edit-workplan":
        attribute = session.split("-")[2]
        workplan = session.split("-")[3]

        if attribute == "name":
            update_workplan_db("name", masukan_raw, workplan)
            reply("Nama Program Kerja telah diperbarui")
            update_session("default")

        elif attribute == "pic":
            update_workplan_db("pic", masukan_raw, workplan)
            reply("Penanggung jawab Program Kerja telah diperbarui")
            update_session("default")

        elif attribute == "descriptions":
            update_workplan_db("descriptions", masukan_raw, workplan)
            reply("Isi Program Kerja telah diperbarui")
            update_session("default")




    # ini di pass karena pada session ini harusnya masuk postback handler
    elif session == "create-schedule-stage-4":
        pass

    elif session == "buat-pengumuman":
        global ANNOUNCEMENT_DETAIL

        if masukan == 'batal':
            reply("Menambahkan pengumuman untuk Grup/Organisasi ini telah dibatalkan")
            ANNOUNCEMENT_DETAIL = None
            update_session("default")

        elif masukan == 'Ya, Lanjutkan!'.lower() and ANNOUNCEMENT_DETAIL != None:
            pengumuman_final = Announcement(id=-1,
                                            title=ANNOUNCEMENT_DETAIL[0],
                                            text=ANNOUNCEMENT_DETAIL[1])
            prosesTambah = add_org_announcement(pengumuman_final, event.source.group_id)
            reply(prosesTambah)
            clear_session_data()
            update_session("default")
            notify_new_announcement(pengumuman_final)

        elif masukan == 'Tidak'.lower() and ANNOUNCEMENT_DETAIL != None:
            reply(
                "Silakan masukkan kembali detail pengumuman. Ketik BATAL untuk membatalkan aksi ini")
            ANNOUNCEMENT_DETAIL = None

        else:
            if len(masukan_raw.split(' | ')) == 2:
                ANNOUNCEMENT_DETAIL = masukan_raw.split(' | ')
                pembuat_pengumuman(event, ANNOUNCEMENT_DETAIL)
            elif len(masukan_raw.split('|')) == 2:
                ANNOUNCEMENT_DETAIL = masukan_raw.split('|')
                pembuat_pengumuman(event, ANNOUNCEMENT_DETAIL)
            else:
                reply(ERROR_WRONG_FORMAT)

    elif session == "buat-proker":
        global PROKER_DETAIL

        if masukan == 'batal':
            reply("Menambahkan proker untuk Grup/Organisasi ini telah dibatalkan")
            PROKER_DETAIL = None
            update_session("default")

        elif masukan == 'Ya, Lanjutkan!'.lower() and PROKER_DETAIL != None:
            proker_final = Workplan(name=PROKER_DETAIL[0],
                                    person_in_charge=PROKER_DETAIL[1],
                                    descriptions=PROKER_DETAIL[2],
                                    is_done="false",
                                    id=-1)
            prosesTambah = add_org_workplan(proker_final, event.source.group_id)
            reply(prosesTambah)
            PROKER_DETAIL = None
            clear_session_data()
            update_session("default")

        elif masukan == 'Tidak'.lower() and PROKER_DETAIL != None:
            reply(
                "Silakan masukkan kembali detail proker. Ketik BATAL untuk membatalkan aksi ini")
            PROKER_DETAIL = None

        else:
            if len(masukan_raw.split(' | ')) == 3:
                PROKER_DETAIL = masukan_raw.split(' | ')
                pembuat_proker(event, PROKER_DETAIL)
            elif len(masukan_raw.split('|')) == 3:
                PROKER_DETAIL = masukan_raw.split('|')
                pembuat_proker(event, PROKER_DETAIL)
            else:
                reply(ERROR_WRONG_FORMAT)

    elif session == "buat-profil":
        global BIODATA_DETAIL

        ids = get_user_organization_ids(event.source.user_id)

        if masukan == 'batal':
            reply("Menambahkan data diri telah dibatalkan")
            BIODATA_DETAIL = None
            update_session("default")

        elif masukan == 'Ya, Lanjutkan!'.lower() and BIODATA_DETAIL != None:
            biodata_final = User(name=BIODATA_DETAIL[1], bod=BIODATA_DETAIL[2],
                                 skill=BIODATA_DETAIL[4],
                                 id=event.source.user_id)
            prosesTambah_profil = modify_user_profile(biodata_final)
            prosesTambah_jabatan = modify_user_position_in_org(event.source.user_id,
                                                               BIODATA_DETAIL[0], BIODATA_DETAIL[3])
            if prosesTambah_profil == "Terima kasih, profil Anda berhasil ditambah/diubah" and prosesTambah_jabatan == "OK":
                reply("Terima kasih, profil Anda berhasil ditambah/diubah")
                BIODATA_DETAIL = None
                clear_session_data()
                update_session("default")
            else:
                reply("Terjadi Kesalahan")

        elif masukan == 'Tidak'.lower() and BIODATA_DETAIL != None:
            reply(
                "Silakan masukkan kembali data diri Anda. Ketik BATAL untuk membatalkan aksi ini")
            BIODATA_DETAIL = None

        else:
            if len(masukan_raw.split(' | ')) == 5:
                BIODATA_DETAIL = masukan_raw.split(' | ')
                if BIODATA_DETAIL[0] in ids:
                    pembuat_profil_nongrup(event, BIODATA_DETAIL)
                else:
                    reply("Token grup salah, Silakan coba kembali")

            elif len(masukan_raw.split('|')) == 5:
                BIODATA_DETAIL = masukan_raw.split('|')
                if BIODATA_DETAIL[0] in ids:
                    pembuat_profil_nongrup(event, BIODATA_DETAIL)
                else:
                    reply("Token grup salah, Silakan coba kembali")

            else:
                reply(ERROR_WRONG_FORMAT)

    else:
        reply(
            "Orgo mengalami masalah teknis, Orgo akan reset, silakan coba kembali sesaat lagi!")
        update_session("default")
        clear_session_data()
